import glob, os
from config_sys import CAMPO_BUSCA, CAMINHO_TEMP, CAMINHO_INSERT


id_licenca = str(CAMPO_BUSCA)
caminho_inserts = CAMINHO_INSERT
caminho_temp = CAMINHO_TEMP

def buscar():
	try:
		arquivo = os.chdir(caminho_temp)
		for f in glob.glob("*.sql"):
			#nome_do_arquivo = "INSERTS_" + os.path.basename(f)
			basename = os.path.basename(f).split(".")
			nome_do_arquivo = "{basename}-{complemento}{extensao}".format(basename=basename[0], complemento="INSERTS",extensao=".sql")
			#print(nome_do_arquivo)
			print(os.path.basename(f))
			tb_tipos_documento(nome_do_arquivo, f);
			#print(os.path.basename(f)+"...............Ok! finalizado")

	except KeyboardInterrupt:
		print("Até mais.")


def tb_tipos_documento(arquivo_escrita, arquivo_leitura):
	try:

		fw = open(caminho_inserts+arquivo_escrita,'w', encoding="utf8")
		f = open(caminho_temp+arquivo_leitura,'r', encoding="utf8")

		for l in f:
			il = metodo_2(l)
			if id_licenca in il:
				fw.write(l)	

		fw.close()		
		f.close()

	except NameError as e:
		print ("Ops... Verifique -> %s" % e)
	except FileNotFoundError as e:
		print ("Arqivo nao encontrado")
	except IndexError as e:
		print ("Ops... Verifique se existe quebra de linha no arquivo:"+caminho_temp+arquivo_leitura)
		print ("... ou se a posicao da licenca esta correta")
	except ValueError:
		print("Ops... verifique se existe quebra de linha no arquivo: "+caminho_temp+arquivo_leitura)
	finally:
		fw.close()		
		f.close()


def retirar_caracteres_indevidos(valores):

	if "'" not in valores:
		return valores
	
	else:

		index_1 = valores.index("'")
		valores_copia_1 = valores[:index_1]+valores[index_1+1:]
		index_2 = valores_copia_1.index("'")+2

		valor_aspas_simples = valores[index_1:index_2]
		valor_aspas_simples = valor_aspas_simples.replace(",","")
		valor_aspas_simples = valor_aspas_simples.replace(" ","")
		valor_aspas_simples = valor_aspas_simples.replace("'","")
		valor_aspas_simples = valor_aspas_simples.replace("(","")
		valor_aspas_simples = valor_aspas_simples.replace(")","")
		valor_aspas_simples = valor_aspas_simples.replace(";","")
		
		valores = valores[:index_1] + valor_aspas_simples + valores[index_2:]
		valores = valores.replace("(","")
		valores = valores.replace(")","")
		valores = valores.replace(";","")
		valores = valores.replace(" ","") 
		
		return retirar_caracteres_indevidos(valores)

def metodo_4(valor_1,valor_2):
	valores_colunas = valor_2.split(",")
	id_licenca = valores_colunas[valor_1]

	return id_licenca

'''
 * valor_1: recebe os nomes das colunas
 * valor_2: recebe os valores das colunas
'''
def metodo_3(valor_1,valor_2):
	colunas = valor_1.split(",")

	contador = 0
	posicao = 0
	for c in colunas:
		if 'id_licenca' in c:
			posicao = contador
		contador += 1
	
	return metodo_4(posicao,valor_2)

def metodo_2(l):
	cols = l.split("VALUES")
	cols_2 = cols[0].split("(")
	del cols_2[0]
	cols_3 = cols_2[0].split(")")
	
	coluna_nome_campos = cols_3[0]
	valores = cols[1]

	nomes_colunas_limpos = coluna_nome_campos.replace(" ","")
	valores_colunas_limpos = retirar_caracteres_indevidos(valores)

	id_licenca = metodo_3(nomes_colunas_limpos, valores_colunas_limpos)
	
	return id_licenca
