1-Incluir o caminho "C:\Program Files\PostgreSQL\9.5\bin" na variável 'path' do sistema Windows;
2-Caso queira a senha do Postgres já configurada, adicione a seguinte linha ao arquivo 'pgpass.conf':
hostname:port:database:username:password
Abra o prompt de comando do Windows e digite o comando para abrir o arquivo '.pgpass':
>cd %APPDATA%\postgresql
>notepad pgpass.conf

OBS: link do tutorial: http://www.douglaspasqua.com/2011/08/27/postgres-tips-arquivo-de-senha-pgpass/