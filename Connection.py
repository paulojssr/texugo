import psycopg2 as p

class Connection:
	"""docstring for ClassName"""
	def __init__(self):
		try:
			print("Estabelecendo conexao...")
			self.connection = p.connect(
				"dbname='teste' user='postgres' host='localhost' password='postgres' port='5432'")
		except Exception as e:
			print("Error %s" %e)
		else:
			#self.connection.autocommit =True 
			self.cursor = self.connection.cursor()
		finally:
			pass

	def execinsert(self, sql):
		try:
			self.cursor.execute(sql)
			self.connection.commit()
			#self.cursor.fetchall()
		except p.DatabaseError as e:
			if self.connection:
				self.connection.rollback()
			print("Error %s" %e)

#conn = Connection()
#conn.execsql()
