import os
from config_sys import CAMINHO_TEMP,TABELAS_DUMP 


#print(TABELAS_DUMP)

tabelas = TABELAS_DUMP
path = CAMINHO_TEMP

def dump():
	cont = 1
	for tabela in tabelas:
		print(tabela)
		backup_nome = "{pos}-{nome}".format(pos=cont, nome=tabela.split('.')[1]+'.sql') # pega a segunda parte da variavel tabela (public.nometabela) para nomear o arquivo backup
		#dump = 'pg_dump.exe --host localhost --port 5432 --username "postgres"  --format plain --section data --column-inserts --file "/Users/paulojorge/Desktop/PedroInacio.sql" --table "public.usuarios" "gnc"'
		dump = 'pg_dump.exe --host localhost --port 5432 --username "postgres"  --format plain --section data --column-inserts --file '+path+backup_nome+' --table '+tabela+' "gnc"'
		os.system(dump)
		cont += 1
