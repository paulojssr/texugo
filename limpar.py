import glob, os
from config_sys import CAMINHO_TEMP
import quebra_linha




def limpar():
	try:
		"""
			os.chdir(CAMINHO_TEMP) -> muda o diretorio atual para o diretorio padrao onde
			estao os arquivos do dump
		"""
		os.chdir(CAMINHO_TEMP)
		for f in glob.glob("*.sql"):
			arq_r = open(CAMINHO_TEMP + f, 'r', encoding="utf8")
			texto_r = arq_r.readlines()
			nome_arq = os.path.basename(f)
			print(nome_arq)
			arq_r.close()
			os.remove(f)

			execl(nome_arq, texto_r)

	except Exception as e:
		print(e)
	finally:
		pass

"""
	*Este metodo e usado para abrir um arquivo no mode='w' (escrita)
	*
"""
def warquivo(nome):
	return open(CAMINHO_TEMP + nome, 'w', encoding='utf8')

"""
	* Metodo pra executar em sequencia os metodo de limpeza
	*
	*
"""
def execl(nome, texto):
	wtexto = []
	for l in texto:
		if linha_branco(l):
			pass
		else:
			if elimina_caracteres(l):
				pass
			else:
				wtexto.append(l)
	quebra_linha(nome, wtexto)


"""
	* METODO PARA VERIFICAR A EXISTENCIA DE LINHAS EM BRANCO
	* Este metodo verifica se o tamanho da linha e igual a 1 'len(l)', se verdadeiro, executa o proximo 'if' verifcando
	* de acordo com a tabela ASCII se o caracter da posicao [0] e do tipo 'mudanca de linha (\n)' - ord(l[0]) == 10 -, retornando 'True'
"""
def linha_branco(linha):
	l = linha
	b = False
	if len(l) == 1:
		if ord(l[0]) == 10: # TABELA ASCII - 10 = '\n'
			b = True
	return b

"""
	*
	*
"""
def elimina_caracteres(linha):
	l = linha
	b = False
	if '--' in l:
		b = True
	elif 'SET ' in l:
		b = True
	elif 'SELECT ' in l:
		b = True
	return b
		
"""
	*
	*
"""
def quebra_linha(nome, texto):
	try:
		fw = warquivo(nome)
	except Exception as e:
		raise
	else:
		txt = texto
		cont = 0
		for l in txt:
			
			if 'INSERT ' not in l:						# caso a linha nao contenha a palavar 'INSERT', significa que exite um '\n' no final da linha anterior
				txt[cont-1] = txt[cont-1].rstrip()+" "  # o metodo rstrip() elimina o '\n' da linha anterior e adiciona um espaco

			cont += 1
		fw.writelines(txt)
	finally:
		fw.close()
	
